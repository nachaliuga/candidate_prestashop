# Uzduotis prestashop

##  gitApie

Dirbo Jonas, su komanda, kūrė jis modulį, modulis turėjo rodytį banerį priskirtai kategorijai, viskas buvo jau beveik pabaigta, bet vieną dieną Jonas taip ir nepasirodė, paliko mus, paliko ir darbus, nuo tos dienos Jono niekas nebematė, kas išgelbės Jono projektą?
Vieną dalyka Jonas padarė gerai, sukūrė konteinerį, kad niekam nekiltu klausimų kaip pasileisti projektą. Jei mus girdi ačiū Jonai.

## Reikalavimai
1. Instaliuoti prestashop, naudojant "docker-compose up" komandą, arba pasileidžiant projektą savais būdais
2. Instaliuoti modulį categorybanner" repozitorijoje pridėtas .zip failas
3. Sekti modulio pakeitimus git repozitorijoje(kad įvykus nelaimei, galetumeme išspresti problemas greitai ir stebint pakeitimus).
4. Sutvarkyti modulį "categorybanner", kad nebūtų gėda bei pavojinga kelti į production serverį

## Reikalavimai modulio atvaizadvimui 
1. Modulis turi leisti pasirinkti kategoriją, kuriai bus priskirtas banneris
2. Baneris turi turėti galimybę būti priskirtas viršuj arba apačioj
3. Banerių liste turi būti galimybė įijungti ir išjungti banerius
4. Baneris turi būti rodomas viršuje(apačioje rodomas lygiai taip kaip viršuje, tik po visų produktų turiniu), formatu kaip nurodyta paveikslėlyje, repozitorijoje 
![alt text](https://gitlab.com/nachaliuga/candidate_prestashop/-/blob/master/banner-top-example.png?raw=true "Pavyzdys")

## Baigus užduotį
* Įkelti pakeistus failus į repozitorija
 
## Pagalba
#####DB
* ```dbhost: prestashop_db```
* ```dbport: 3306```
* ```dbuser: docker```
* ```dbpassw: docker```
#####Komanda paleisti projektą
* ``docker-compose up``
#####Komanda išjungti projektą
* ``docker-compose down``

### Prestashop prisijungimai
* ```prestaweb: http://localhost:8084/```
* ```admin login : test@test.com```
* ```PW 123456789```
* ```admin url: http://localhost:8084/admin055opbg9w/```

##Kilus klausimams
#####Rašyti i el paštą
[artazzs1@gmail.com](mailto:artazzs1@gmail.com?subject=candidate_prestashop)
