<?php return array (
  'parameters' => 
  array (
    'database_host' => 'prestashop_db',
    'database_port' => '3306',
    'database_name' => 'prestashop',
    'database_user' => 'docker',
    'database_password' => 'docker',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'fvOweQK1FUXEr1yTtSUFNTPEtHKq3OCaSHHTAECINKgBt25FIVwwZmeW',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2020-05-25',
    'locale' => 'en-US',
    'use_debug_toolbar' => true,
    'cookie_key' => 'CKBrcPPhqnvD5GJfKXW1E5htnvWS3qfmBZZz2wBF9sBwWri0zT1Xir9E',
    'cookie_iv' => '155VvhZz',
    'new_cookie_key' => 'def00000aef41addf611c594c7af00effa9b2615c3dd22b0b658554c5add6c00297f9cdedd7372b11f8369aaf7bb5646dad6eda05d2824f687f1f89e45cce293f19e8d61',
  ),
);
